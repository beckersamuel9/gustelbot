# Name
Gustelbot

# Description
My personal twitch bot

# Installation
Requirements:
- Golang 1.18+

Install:
- Clone the repo
- Install the requirements listed in go.mod and go.sum using Go's package management system
- Ready to run

# Usage
The bot requires a json file containing the authentication data and the initial channel.  
This file must contain the following content:  
```json
{
    "token": "Your access token here",
    "refresh": "Your refresh token here",
    "client": "Your client id here",
    "channel": "Name of the inital channel here"
}
```
Refresh is not required to have any valid data as it is only used for storing the refresh token that twitchtokengenerator uses.  
You can get the required data easily by using this [website](twitchtokengenerator.com/), but do mind the warning on it.  

# Project status
Personal project. Probably will do some stuff in it and then never touch it again. Might also become a long term thing I keep improving. Idk yet
