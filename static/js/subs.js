var events_todo = []
var lastUserName = ""

window.onload = startup

function startup() {
    setInterval(fetchSubs, 500);
    setInterval(updateUI, 2500);
}

function hideContent() {
    //var wrapper = document.getElementById("wrapper");
    //wrapper.style.display = "none";
    //var image = document.getElementById("image");
    var body = document.getElementById("wrapper");
    body.style.display = "none";
}

function showContent(newName) {
    var user = document.getElementById("user");
    var body = document.getElementById("wrapper");
    user.innerText = newName;
    body.style.display = "block";
}

function fetchSubs() {
    fetch(encodeURI("/data/sub"))
        .then(res => res.json())
        .then((out) => {
            //console.log("got new event")
            if (out.Name != lastUserName) {
                lastUserName = out.Name;
                events_todo.push(out);
            }
        })
}

function updateUI() {
    var event = events_todo.shift()
    if (event) {
        lastUserName = event.Name;
        showContent(event.Name);
        setTimeout(hideContent, 2000);
    }
}