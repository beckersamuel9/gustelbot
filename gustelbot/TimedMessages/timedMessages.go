package TimedMessages

import (
	"time"
)

type TimedMessage struct {
	Message string
	diff    int64
	send    chan string
	Quit    chan any
}

// Run starts the loop that will send the message stored in the passed TimedMessage periodically.
func (t *TimedMessage) Run() {
	ticker := time.NewTicker(time.Duration(t.diff) * time.Second)
	go func() {
		for {
			select {
			case <-ticker.C:
				t.send <- t.Message
			case <-t.Quit:
				ticker.Stop()
				return
			}
		}
	}()
}

// Stop stops the loop that is started via Run.
func (t *TimedMessage) Stop() {
	t.Quit <- 1
}

func BuildTimedMessage(msg string, pause int64, sendChan chan string) *TimedMessage {
	return &TimedMessage{
		Message: msg,
		diff:    pause,
		send:    sendChan,
		Quit:    make(chan any),
	}
}
