package Commands

import (
	"bufio"
	"errors"
	"os"
	"strings"
)

func loadInfoCommands() (map[string]string, error) {
	handle, err := os.Open("infoCommands.txt")
	if err != nil {
		return nil, err
	}

	if !isFileValid(handle) {
		handle.Close()
		return nil, errors.New("file invalid")
	}

	scanner := bufio.NewScanner(handle)
	cmds := make(map[string]string)

	for scanner.Scan() {
		text := scanner.Text()
		keyval := strings.Split(text, ";;")
		if len(keyval) != 2 {
			handle.Close()
			return nil, errors.New("file invalid")
		}
		cmds[keyval[0]] = keyval[1]
	}

	handle.Close()
	return cmds, nil
}

func isFileValid(handle *os.File) bool {
	info, err := handle.Stat()
	if err != nil {
		return false
	}
	if info.Size() < 5 {
		return false
	}

	return true
}
