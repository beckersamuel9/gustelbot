package Commands

import (
	"log"

	twitch "github.com/gempir/go-twitch-irc/v3"
)

const (
	PermNone = iota
	PermStream
	PermMod
	PermSub
)

type Permission int

type Command struct {
	name    string
	MinArgs int
	MaxArgs int
	perm    Permission
	help    string
	Handler func(*CommandHandler, twitch.PrivateMessage, []string)
}

// MakeCommand generates a new Command struct that contains all info necessary for the bot to use it.
// After making a new one you should add the new command to the bot.
// A command receives a slice of strings representing the arguments when it's called.
// These arguments are similar to argv when calling something from the console.
// The first argument is the command name/alias.
// It is guaranteed that at least minArgs+1 (because of the command name) arguments are present and at most maxArgs+1.
// Arguments are space seperated and excess arguments are joined together to form the final argument.
func MakeCommand(name string, minArgs, maxArgs int, perm Permission, help string, handler func(*CommandHandler, twitch.PrivateMessage, []string)) *Command {
	return &Command{
		name:    name,
		MinArgs: minArgs,
		MaxArgs: maxArgs,
		perm:    perm,
		help:    help,
		Handler: handler,
	}
}

func lockedCommands() map[string]*Command {
	var commands = make(map[string]*Command, 1)

	commands["ping"] = MakeCommand("ping",
		0,
		0,
		PermNone,
		"Check if the bot is currently live. If you see this response, it probably is",
		func(ch *CommandHandler, pm twitch.PrivateMessage, s []string) {
			log.Println("pm.Channel == ch.Channel: ", pm.Channel == ch.Channel)
			ch.Client.Say(ch.Channel, "Pong")
			ch.Client.Reply(ch.Channel, pm.ID, "Pong")
		},
	)
	commands["source"] = MakeCommand("source",
		0,
		0,
		PermNone,
		"Get the link to the bot's source code",
		func(ch *CommandHandler, pm twitch.PrivateMessage, s []string) {
			ch.Client.Reply(ch.Channel, pm.ID, "The bot's source code can be found here: https://gitlab.com/beckersamuel9/gustelbot")
		},
	)
	// Help: Get a helpful description for a command
	commands["help"] = MakeCommand("help",
		1,
		1,
		PermNone,
		"You think you're funny huh?",
		func(ch *CommandHandler, pm twitch.PrivateMessage, s []string) {
			name := s[1]
			if cmd, ok := ch.locked[name]; ok {
				ch.Client.Reply(ch.Channel, pm.ID, cmd.help)
				return
			}
			if cmd, ok := ch.commands[name]; ok {
				ch.Client.Reply(ch.Channel, pm.ID, cmd.help)
				return
			}
			ch.Client.Reply(ch.Channel, pm.ID, "There is no interactive command with that name")
		},
	)
	commands["addInfo"] = MakeCommand("addInfo",
		2,
		2,
		PermMod,
		"Add/overwrite an info command",
		func(ch *CommandHandler, pm twitch.PrivateMessage, s []string) {
			ch.infoCommands[s[1]] = s[2]
		},
	)
	commands["delInfo"] = MakeCommand("delInfo",
		1,
		1,
		PermMod,
		"Remove an info command",
		func(ch *CommandHandler, pm twitch.PrivateMessage, s []string) {
			delete(ch.infoCommands, s[1])
		},
	)
	return commands
}
