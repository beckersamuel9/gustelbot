package Commands

import (
	"log"
	"strings"

	twitch "github.com/gempir/go-twitch-irc/v3"
)

type CommandHandler struct {
	Client       *twitch.Client
	Channel      string
	Botname      string
	Prefix       string
	commands     map[string]*Command
	infoCommands map[string]string
	locked       map[string]*Command
}

// AddCommand adds a new command to the CommandHandler it has been called with.
func (h *CommandHandler) AddCommand(name string, command *Command) {
	h.commands[name] = command
}

// Handle is a callback function that takes care of incoming chat messages and executes commands if a message contains one.
func (h *CommandHandler) Handle(message twitch.PrivateMessage) {
	// Ignore messages that are either from this or other bots or don't have the preset command prefix
	if message.User.Name == h.Botname ||
		strings.HasSuffix(message.User.Name, "bot") ||
		!strings.HasPrefix(message.Message, h.Prefix) ||
		message.Channel != h.Channel {
		return
	}
	var rawCommand = strings.TrimPrefix(message.Message, h.Prefix)
	log.Println(message.User.Name, rawCommand)
	var args = strings.Split(rawCommand, " ")
	// First check if command is part of the locked ones
	if locked, ok := h.locked[args[0]]; ok {
		log.Println("Found locked cmd: ", args[0])
		h.checkPermsAndRun(message, args, locked)
		return
	}
	// If not, check commands given to the command handler
	if cmd, ok := h.commands[args[0]]; ok {
		h.checkPermsAndRun(message, args, cmd)
		return
	}
	// Lastly, check info commands if there is a matching name
	if info, ok := h.infoCommands[args[0]]; ok {
		h.Client.Reply(h.Channel, message.ID, info)
	}
}

func (h *CommandHandler) checkArgsAndRun(msg twitch.PrivateMessage, args []string, cmd *Command) {
	if len(args)-1 >= cmd.MaxArgs {
		// Fix the args since they are space seperated without caring for any special thing
		fixedArgs := make([]string, 0)
		// Add all arguments before maxArgs to the fixed args
		for i := 0; i < cmd.MaxArgs-1; i++ {
			fixedArgs = append(fixedArgs, args[i])
		}
		// If there are only maxArgs arguments available, just append the last one
		if len(args)-1 == cmd.MaxArgs {
			fixedArgs = append(fixedArgs, args[cmd.MaxArgs])
		} else {
			// Else join every argument from maxArgs to end together in a single string
			fixedArgs = append(fixedArgs, strings.Join(args[cmd.MaxArgs:], " "))
		}
		log.Println("Running with fixed args:", fixedArgs)
		cmd.Handler(h, msg, fixedArgs)
	} else {
		h.Client.Reply(h.Channel, msg.ID, "Not enough arguments for command "+args[0])
	}
}

// checkPermsAndRun confirms that the user executing a command has the permissions to do so.
// If they do, the command will be checked for the correct argument count and run.
// If they don't the bot will reply with a descriptive error message.
func (h *CommandHandler) checkPermsAndRun(msg twitch.PrivateMessage, args []string, cmd *Command) {
	switch cmd.perm {
	case PermStream:
		if msg.User.Name == msg.Channel {
			log.Println("Is streamer command, running")
			h.checkArgsAndRun(msg, args, cmd)
			return
		} else {
			h.Client.Reply(h.Channel, msg.ID, "Only the streamer can use this command")
			return
		}
	case PermMod:
		if msg.Tags["mod"] == "1" {
			log.Println("Is mod command, running")
			h.checkArgsAndRun(msg, args, cmd)
			return
		} else {
			h.Client.Reply(h.Channel, msg.ID, "Only moderators and the streamer can use this command")
			return
		}
	case PermSub:
		if msg.Tags["subscriber"] == "1" {
			log.Println("Is sub command, running")
			h.checkArgsAndRun(msg, args, cmd)
			return
		} else {
			h.Client.Reply(h.Channel, msg.ID, "Only subscribers, mods and the streamer can use this command")
			return
		}
	}
	log.Println("Is public command, running")
	h.checkArgsAndRun(msg, args, cmd)
}

func NewCommandHandler(client *twitch.Client, channel, name, prefix string) *CommandHandler {
	infoCommands, err := loadInfoCommands()
	if err != nil {
		log.Printf("Couldn't load info commands stored in file infoCommands.txt. Error: %s", err)
		infoCommands = make(map[string]string)
	}

	handler := &CommandHandler{
		Client:       client,
		Channel:      channel,
		Botname:      name,
		Prefix:       prefix,
		commands:     make(map[string]*Command),
		infoCommands: infoCommands,
		locked:       lockedCommands(),
	}
	return handler
}
