package gustelbot

import (
	"log"
	"time"

	"github.com/gempir/go-twitch-irc/v3"
	"gitlab.com/beckersam/gustelbot/gustelbot/Commands"
	"gitlab.com/beckersam/gustelbot/gustelbot/TimedMessages"
	"gitlab.com/beckersam/gustelbot/gustelbot/utility"
	"gitlab.com/beckersam/gustelbot/server"
)

type selfInfo struct {
	Name    string
	Channel string
	Auth    *utility.AuthData
}

type Bot struct {
	Handler     *Commands.CommandHandler
	Messages    []*TimedMessages.TimedMessage
	sendMsgChan chan string
	Client      *twitch.Client
	auth        *utility.AuthData
	Web         server.WebInterface
	Info        selfInfo
}

// Build a new chatbot for a singular channel.
func BuildBot(name, channel string, auth *utility.AuthData, handler *Commands.CommandHandler) *Bot {
	client := twitch.NewClient(name, auth.Token)
	client.OnPrivateMessage(handler.Handle)

	subChan := make(chan server.LastXyz)
	followChan := make(chan server.LastXyz)
	client.OnUserNoticeMessage(func(message twitch.UserNoticeMessage) {
		newEvent := server.NewLastXyz(message.User.DisplayName, time.Now())
		log.Println(newEvent)
		subChan <- *newEvent
	})

	client.OnConnect(func() { log.Println("Connected to twitch") })

	client.Join(channel)

	return &Bot{
		Handler:     handler,
		Messages:    make([]*TimedMessages.TimedMessage, 0),
		sendMsgChan: make(chan string),
		Client:      client,
		auth:        auth,
		Web:         *server.NewWebInterface(subChan, followChan),
	}
}

// Start starts both the twitch bot and the webserver for web embeds.
// If either the server or the bot run into an error and return the error will be returned.
func (b *Bot) Start() error {
	// Run everything as a goroutine and collect errors in a channel
	errChan := make(chan error)
	go b.Web.Run(errChan)
	go b.Run(errChan)
	go b.timedMessageHandler()

	for err := range errChan {
		return err
	}
	return nil
}

func (b *Bot) Run(errChan chan error) {
	errChan <- b.Client.Connect()
}

func (b *Bot) AddTimedMessage(msg string, secDiff int) {
	tMsg := TimedMessages.BuildTimedMessage(msg, int64(secDiff), b.sendMsgChan)
	b.Messages = append(b.Messages, tMsg)
	tMsg.Run()
}

func (b *Bot) timedMessageHandler() {
	for msg := range b.sendMsgChan {
		b.Client.Say(b.auth.Channel, msg)
	}
}

// client.OnUserNoticeMessage(func(message twitch.UserNoticeMessage) {
// 	newEvent := server.NewLastXyz(message.User.DisplayName, time.Now())
// 	log.Println(newEvent)
// 	subChannel <- *newEvent
// 	/*
// 		{
// 			// User
// 			{
// 				137271740
// 				sineralyn
// 				SineraLyn
// 				#1E90FF
// 				map[subscriber:6]
// 			}
// 			// Type
// 			4
// 			// Raw type
// 			USERNOTICE
// 			// Tags
// 			map[
// 				badge-info:subscriber/10
// 				badges:subscriber/6
// 				color:#1E90FF
// 				display-name:SineraLyn
// 				emotes:
// 				flags:
// 				id:f5c9107e-5178-49ca-8c7e-a36ccabeb976
// 				login:sineralyn
// 				mod:0
// 				msg-id:resub
// 				msg-param-cumulative-months:10
// 				msg-param-months:0
// 				msg-param-multimonth-duration:0
// 				msg-param-multimonth-tenure:0
// 				msg-param-should-share-streak:0
// 				msg-param-sub-plan:1000
// 				msg-param-sub-plan-name:Chatblin
// 				msg-param-was-gifted:false
// 				room-id:452746699
// 				subscriber:1
// 				system-msg:SineraLyn subscribed at Tier 1. They've subscribed for 10 months!
// 				tmi-sent-ts:1657457875715
// 				user-id:137271740
// 				user-type:
// 			]
// 			// Channel
// 			dyarikku
// 			// Room ID
// 			452746699
// 			// ID
// 			f5c9107e-5178-49ca-8c7e-a36ccabeb976
// 			// Time
// 			2022-07-10 14:57:55.715 +0200 CEST
// 			// Emotes
// 			[]
// 			// MessageID
// 			resub
// 			// Message params
// 			map[
// 				msg-param-cumulative-months:10
// 				msg-param-months:0
// 				msg-param-multimonth-duration:0
// 				msg-param-multimonth-tenure:0
// 				msg-param-should-share-streak:0
// 				msg-param-sub-plan:1000
// 				msg-param-sub-plan-name:Chatblin
// 				msg-param-was-gifted:false
// 			]
// 			// Message
// 			SineraLyn subscribed at Tier 1. They've subscribed for 10 months!
// 		}
// 	*/
// })
