package utility

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

type AuthData struct {
	Token    string `json:"token"`
	Refresh  string `json:"refresh"`
	ClientID string `json:"client"`
	Channel  string `json:"channel"`
}

func NewAuthData(filename string) *AuthData {
	handle, err := os.Open(filename)
	check(err)
	defer handle.Close()

	var rawContent []byte
	rawContent, err = ioutil.ReadAll(handle)
	check(err)
	var auth AuthData

	err = json.Unmarshal(rawContent, &auth)
	check(err)

	auth.Token = "oauth:" + auth.Token

	return &auth
}
