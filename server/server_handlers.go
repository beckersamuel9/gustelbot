package server

import "net/http"

func HandlerHelper(path string, handler func(http.ResponseWriter, *http.Request)) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path != path {
			http.Error(w, "404 not found.", http.StatusNotFound)
			return
		}
		handler(w, r)
	}
}
