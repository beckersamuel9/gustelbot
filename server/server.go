package server

import (
	"encoding/json"
	"log"
	"net/http"
	"time"
)

type LastXyz struct {
	Name string
	time time.Time
	Time int64
}

func NewLastXyz(name string, eTime time.Time) *LastXyz {
	now := time.Now()
	return &LastXyz{
		Name: name,
		time: now,
		Time: int64(now.UnixMilli()),
	}
}

type WebInterface struct {
	lastSub       LastXyz
	lastFollow    LastXyz
	subChannel    chan LastXyz
	followChannel chan LastXyz
}

func (wi *WebInterface) Run(errChan chan error) {
	fileServer := http.FileServer(http.Dir("./static"))
	http.Handle("/", fileServer)
	http.HandleFunc("/data/sub", HandlerHelper("/data/sub", func(w http.ResponseWriter, r *http.Request) {
		data, err := json.Marshal(wi.lastSub)
		if err != nil {
			http.Error(w, "Error encoding lastSub data: "+err.Error(), http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(data)
	}))

	log.Println("Starting server at port 8080")
	go wi.updateEvents()
	errChan <- http.ListenAndServe(":8080", nil)
}

func (w *WebInterface) updateEvents() {
	for {
		select {
		case newSub := <-w.subChannel:
			w.lastSub = newSub
		case newFollow := <-w.followChannel:
			w.lastFollow = newFollow
		}
	}
}

func NewWebInterface(subChannel, followChannel chan LastXyz) *WebInterface {
	return &WebInterface{
		subChannel:    subChannel,
		followChannel: followChannel,
	}
}
