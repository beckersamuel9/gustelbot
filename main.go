package main

import (
	twitch "github.com/gempir/go-twitch-irc/v3"
	"gitlab.com/beckersam/gustelbot/gustelbot/Commands"
	"gitlab.com/beckersam/gustelbot/gustelbot/gustelbot"
	"gitlab.com/beckersam/gustelbot/gustelbot/utility"
)

// Modules that I will need later on
// require (
// 	github.com/faiface/beep v1.1.0 // indirect
// 	github.com/golang-jwt/jwt v3.2.1+incompatible // indirect
// 	github.com/nicklaw5/helix v1.25.0 // indirect
// )

func main() {
	test()
}

// Simple test bot setup without any fancy message sorting
func test() {
	var auth = utility.NewAuthData("testbot.json")

	client := twitch.NewClient("mrevilsbot", auth.Token)

	client.OnPrivateMessage(func(message twitch.PrivateMessage) {
		if message.Message == "?ping" {
			client.Reply("mrevilontwitch", message.ID, "pong")
		}
	})

	client.Join("mrevilontwitch")

	a := make(chan error)
	go func() {
		err := client.Connect()
		if err != nil {
			a <- err
		}
	}()

	err := <-a
	if err != nil {
		panic(err)
	}
}

// The original content of the main function. Launching everything
func oldMain() {
	var auth = utility.NewAuthData("testbot.json")

	client := twitch.NewClient("mrevilsbot", auth.Token)
	var commandHandler = Commands.NewCommandHandler(client, auth.Channel, "mrevilsbot", "?")
	var bot = gustelbot.BuildBot("mrevilsbot", auth.Channel, auth, commandHandler)

	bot.Client.OnRoomStateMessage(func(message twitch.RoomStateMessage) { bot.Client.Say(auth.Channel, "Bot online") })

	// Bunch of different methods to start the twitch bot

	// The bot wrapper's internal starting point
	//ch := make(chan error)
	//go bot.Run(ch)
	//err := <-ch

	// Directly starting just the bot stored in the wrapper
	//err := bot.Client.Connect()

	// Start method of the wrapper. Includes webserver
	err := bot.Start()
	if err != nil {
		panic(err)
	}
}
